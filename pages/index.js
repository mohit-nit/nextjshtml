import Head from "next/head";
import Script from "next/script";
import Image from "next/image";
import InfiniteScroll from "react-infinite-scroll-component";
import Bottom from "../components/Bottom";
import Content from "../components/Content";
import Content2 from "../components/Content2";
import Content3 from "../components/Content3";
import Content4 from "../components/Content4";
import Content5 from "../components/Content5";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";

export default function Home() {
  const handleScript = () => {
    console.log("Its working");
    if (typeof window !== "undefined") {
      window.googletag = window.googletag || { cmd: [] };
      googletag.cmd.push(function () {
        googletag
          .defineSlot(
            "/22029514685/ca-pub-3859189003926819-tag/BuzzTribe_Test_1",
            [
              [300, 250],
              [336, 280],
            ],
            "div-gpt-ad-1"
          )
          .addService(googletag.pubads());

        googletag
          .defineSlot(
            "/22029514685/ca-pub-3859189003926819-tag/BuzzTribe_Test_2",
            [
              [300, 250],
              [336, 280],
            ],
            "div-gpt-ad-2"
          )
          .addService(googletag.pubads());

        googletag
          .defineSlot(
            "/22029514685/ca-pub-3859189003926819-tag/BuzzTribe_Test_3",
            [
              [300, 250],
              [336, 280],
            ],
            "div-gpt-ad-3"
          )
          .addService(googletag.pubads());

        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
      });
    }
  };
  return (
    <div className="body">
      <Head>
        <title>Buzztribe News</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
        <Script
          async="async"
          src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"
        />
        {/* <script>
                    {`var googletag = googletag || {};
                     googletag.cmd = googletag.cmd || [];`}
                </cript> */}
        <Script>{handleScript()}</Script>
      </Head>
      <Navbar />

      <div className="bodyContainer">
        <Content />
        {/* <Content2/>
        <Content3/>
        <Content4/>
        <Content5/> */}
      </div>
      <Bottom />
    </div>
  );
}
