import React, { useEffect } from "react";
import Footer from "./Footer";
import Aos from "aos";
import "aos/dist/aos.css";
import Head from "next/head";
import Script from "next/script";
import { useRouter } from "next/router";
import AdSlot from "./AdSlot";
import * as gtag from "../lib/gtag";

const Content = ({ Component, pageProps }) => {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);

  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);
  return (
    <div>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>NextJs Practise</title>
        <script data-ad-client="ca-pub-3859189003926819" 
          async="async"
          src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
        />
        <script
          strategy="afterInteractive"
          src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
        />
        <script
          id="gtag-init"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${gtag.GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });`,
          }}
        />
      </Head>

      <div className="article">
        {/* <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div> */}
        <div
          id="div-gpt-ad-3"
          style={{ minWidth: "300px", minHeight: "250px" }}
        >
          <script data-ad-client="ca-pub-3859189003926819" 
          async="async"
          src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
        />
        </div>

        <div className="blog-post">
          <div className="featured-img">
            <img
              style={{ width: "100%" }}
              src="images/The-art-of-listening-and-what-it-means-to-be-a-good-listener.webp"
            />
          </div>

          <div className="post-header">
            {/* <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div> */}
            {/* <div
              id="div-gpt-ad-2"
              style={{ minWidth: "100px", minHeight: "50px" }}
            >
              <script>
                {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-2'); });`}
              </script>
            </div> */}

            <p className="category">Life Style</p>
            <h1>
              The art of listening, and what it means to be a good listener
            </h1>
            <img src="images/logo_glance.png" className="glance-logo" />
          </div>

          <div className="post-content">
            <p>
              <em>
                The best-gifted humans are bestowed upon but not used fully
              </em>
            </p>
            <p>
              Among the various naturally inbuilt skills such as talking, touch,
              emotions, those humans possess one which is the easiest
              and&nbsp;difficult at the same time is listening. A skill or a
              natural ability involving the easiest effort of just lending one’s
              ear to a conversation may seem the easiest but requires one’s
              utmost attention and time. Listening is a skill and quality that
              makes for a good communicator. To understand it better, the art of
              listening forms the basis of great communication. For if you have
              the ability to listen, absorb the words thrown at you, only then
              will you be able to contribute to a conversation.
            </p>
            <p>
              <strong>What really does it mean to be a good listener? </strong>
            </p>
            <p>
              Sitting among a group of people conversing on random topics and
              hardly being a part of the conversation happening doesn’t make one
              a listener. To be a good listener requires patients and the desire
              to be in the space and absorb the word exchange. Believe it or
              not, being a good listener isn’t easy and requires a lot of hard
              work.
            </p>
            <p>
              According to various studies, an average person can remember only
              50 per cent of the conversations exchanged or held. A few other
              studies have also proven that this calculation is a result of the
              fact that listening is considered a passive process that requires
              no effort for people.
            </p>
            <p>
              <strong>Here is how one can master the art of listening</strong>
            </p>
            <p>
              Often during conversations, in a group or one-on-one, people
              listen to the words without truly gauging the meaning behind them
              or drawing any sense from them. The reason being, occupied mind
              wandering away to far of places or the lack of interest in the
              words being exchanged. Another reason that can contribute to the
              lack of ability to listen is the constant urge humans possess to
              speak. Listening is an art that demands receptive, open-minded,
              and non-judgmental attention. It is in itself mediation as it
              requires you to be still but mind active.
            </p>
            <p>
              Be patient and don’t interrupt when someone is talking. This is
              the first and foremost skill of defining a good listener. Being
              just there in the conversation, listening, and keeping a note of
              things only to speak after the person talking has finished is the
              mark of a good listener.
            </p>
            <p>
              Make sure to make eye contact. This displays your presence in the
              conversation and shows the respect you have to offer to the
              communicator.
            </p>
          </div>
          <Footer />
        </div>
      </div>
      <div data-aos="slide-up" className="article">
        {/* <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div> */}
        {/* <div
          id="div-gpt-ad-3"
          style={{ minWidth: "300px", minHeight: "250px" }}
        >
          <script>
            {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-3'); });`}
          </script>
        </div> */}
        <div className="blog-post">
          <div className="featured-img">
            <img
              style={{ width: "100%" }}
              src="images/7-Easy-Hacks-to-Decorate-Your-Room-for-the-Festive-Season.webp"
            />
          </div>

          <div className="post-header">
            {/* <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div> */}
            <div
              id="div-gpt-ad-1"
              style={{ minWidth: "100px", minHeight: "50px" }}
            >
              <script>
                {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-1'); });`}
              </script>
            </div>

            <p className="category">Life Style</p>
            <h1>Christmas Decor That Can Work All Year</h1>
            <img src="images/logo_glance.png" className="glance-logo" />
          </div>

          <div className="post-content">
            <p>
              <em>Keep up the spirit all through the year!</em>
            </p>
            <p>
              One can never get over the heart fulfilling and joy spreading
              Christmas decoration. So, why stop at just Christmas? You can
              ensure that your house is sprinkled with the happy and celebratory
              vibes of the festival throughout the year without coming across
              the crazy person who forgot to take their lights down. In fact,
              you keep your Christmas decoration at the back of your shelf all
              year only to be used once in a year and that never really makes
              sense. Think about it –if you had decor pieces that could
              perfectly work with all seasons and throughout the year, you will
              get all your money’s worth out of them. Here are a few Christmas
              decor ideas that you can use all throughout the year.
            </p>
            <h6>The Cozy Glow Of The Twinkling Lights</h6>
            <p>
              If you are one of those people who absolutely love the warm and
              cosy hanging lights during Christmas, here’s the good news: you
              can use them all year. The hanging Christmas tree lights could be
              used all year round but of course in different manners. For
              example, you can use them in your bedroom for a little warm and
              ambient lighting in the night. You can also use them to hang over
              patio or deck where you can have a romantic dinner or two. One
              more way you can use them is to turn your kid’s room into a fairy
              wonderland.
            </p>
            <h6>Keeping The Candles Burning All Year</h6>
            <p>
              For a change, may be don’t just store away your beautiful
              Christmas candles this year? It would be best if you get the
              candles in the versatile white colour instead of the clichéd red
              and green. That way, you can use the candles all the year long.
              You can use it for a perfect candle light dinner with your date.
              You can use it during your meditation when you simply just need to
              relax, focus and be in the moment. You can also get scented
              candles for the same purpose and they would work great Christmas
              or otherwise.
            </p>
            <h6>Metallic Pieces Of Decor For All Seasons</h6>
            <p>
              May be don’t go for the red and green decor pieces at all! Use
              metallic colours like silver that can work very well for winter
              and all through the year. Moreover, pairing metallic colours with
              neutral themes around the house will amp up the aesthetics of the
              space.
            </p>
          </div>
          <Footer />
        </div>
      </div>
      <div data-aos="slide-up" className="article">
        {/* <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div> */}
        {/* <div
          id="div-gpt-ad-3"
          style={{ minWidth: "300px", minHeight: "250px" }}
        >
          <script>
            {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-3'); });`}
          </script>
        </div> */}
        <div className="blog-post">
          <div className="featured-img">
            <img
              style={{ width: "100%" }}
              src="images/GO-BANANA-4-DESSERT-MADE-OUT-OF-BANANA.webp"
            />
          </div>

          <div className="post-header">
            {/* <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div> */}
            {/* <div
              id="div-gpt-ad-3"
              style={{ minWidth: "100px", minHeight: "50px" }}
            >
              <script>
                {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-3'); });`}
              </script>
            </div> */}
            <p className="category">Food</p>
            <h1>Desserts That Will Not Make You Fat</h1>
            <img src="images/logo_glance.png" className="glance-logo" />
          </div>

          <div className="post-content">
            <p>
              <em>Indulge in some desserts guilt free!</em>
            </p>
            <p>
              For every time that you have stopped yourself from taking that
              bite out of a good piece of chocolate cake, you can now make up to
              them. There’s nothing wrong with having indulging in some dessert
              even though you are looking to maintain or lose your weight. There
              are plenty of desserts out there that even people who gain weight
              by just inhaling air can enjoy. The obvious trick is to moderate
              your portions. However, if you are still worried about the empty
              calories, we have some news for you. There are desserts, and we
              say this with conviction, that will not make you fat. Here are a
              few desserts that you can enjoy without having to worry about
              gaining those extra pounds.
            </p>
            <h6>Bananas Dipped In Dark Chocolate</h6>
            <p>
              You want some health and taste in your dessert? Go with bananas
              and of course, chocolate.&nbsp; Bananas have incredible benefits
              for your health and they have the sweetness to them which would
              take care of the no sugar part in your healthy, guilt free
              dessert. The sinfulness like in any good dessert would of course
              come from the dark chocolate. Just dip some bananas in dark
              chocolate that’s melted and mixed with almonds and walnuts. Follow
              this step by refrigerating the dark chocolate coated bananas like
              a popsicle for an hour or two and your dessert will be ready.
            </p>
            <h6>A Fudgy Delight For A Sinless Dessert</h6>
            <p>
              Who said fudgy brownies are bad for losing weight? We say have all
              the fudge in your life as you would like but with a twist. Make
              the fudgy brownies with a healthy amount of raspberries and some
              lemon zest for garnish. You use applesauce for sweetness and whole
              wheat flour. That instantly makes it a lot lighter in terms if
              calories compared to your normal brownies. Moreover, there’s no
              compromise in taste as you get big and bold flavours that could
              even possibly send you to a dessert coma. You indulge while still
              maintaining your nutrition and calorie goals.
            </p>
            <h6>All the Goodness Of The Carrots</h6>
            <p>
              Okay, we agree that a little bit of sugar is important in carrot
              cakes, but that amount barely does any harm. Instead of using the
              cream cheese frosting use Greek yoghurt for the frosting and the
              carrot cake will taste a hundred percent as good as your OG
              grandma special carrot cake, we promise!
            </p>
          </div>

          <Footer />
        </div>
      </div>
      <div data-aos="slide-up" className="article">
        {/* <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div> */}
        {/* <div
          id="div-gpt-ad-3"
          style={{ minWidth: "300px", minHeight: "250px" }}
        >
          <script>
            {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-3'); });`}
          </script>
        </div> */}
        <div className="blog-post">
          <div className="featured-img">
            <img
              style={{ width: "100%" }}
              src="images/Useful-tips-for-buying-used-furniture.webp"
            />
          </div>

          <div className="post-header">
            {/* <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div> */}
            {/* <div
              id="div-gpt-ad-2"
              style={{ minWidth: "100px", minHeight: "50px" }}
            >
              <script>
                {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-2'); });`}
              </script>
            </div> */}
            <p className="category">Life Style </p>
            <h1> How To Care For Velvet Furniture</h1>
            <img src="images/logo_glance.png" className="glance-logo" />
          </div>

          <div className="post-content">
            <p>
              <em>
                When you get a fancy piece of furniture, you need to give it the
                fancy treatment.
              </em>
            </p>
            <p>
              Velvet is universally the most stylish and elite options for
              furniture of all times. It gives a little vintage, a little boho
              and a lot of glam to the look of your space. However, velvet
              furniture is also not the easiest to maintain especially if you
              are new to it. In fact, even people who have been donning velvet
              furniture for years haven’t got it all right. It is a little
              difficult to keep in the mint condition. Velvet takes extra and
              special care and rightfully so. Velvet comes with a price of
              labour but not something that would make you want to shoot your
              brains out. Here are a few tips on how to take care of velvet
              furniture.
            </p>
            <h6>Keep It Simple In General</h6>
            <p>
              Quite expectedly you will not be able to keep your velvet
              furniture safe from the usual dirt and debris. In such a case keep
              the cure usual and simple as well. Simply use a vacuum cleaner to
              clean the fabric. Make sure to vacuum your velvet furniture at
              least once a week so as to not let the dirt on it for too long. If
              not cleaned with a vacuum cleaner at least once a week, the dirt
              will sit too long on the fabric and soon sediment itself on it. If
              that happens, it would be a lot difficult for you to clean the
              dirt out even with a vacuum cleaner.
            </p>
            <h6>Speaking For The Spills, Speed It Up</h6>
            <p>
              In case there are any spills on you velvet furniture, you need to
              act fast. Use a clean paper towel or a cloth with good absorbing
              powers to soak the spill up. However, while you are at it,
              remember not to dab the spill or rub on it. Doing this will push
              the liquid further into the deeper layers of the fabric and its
              fibres. Leave the soaking cloth on the spill for as long as it
              takes to soak up the entire liquid. Once that’s done, let the
              fabric air dry.
            </p>
            <h6>Keep The Sunlight Away</h6>
            <p>
              Velvet is very sensitive to sunlight. If kept in direct sunlight,
              the velvet’s colour would fade away in a matter of time. Select a
              space with low sunlight for your velvet furniture or at least
              cover it with a throw for protecting its colour.
            </p>
          </div>

          <Footer />
        </div>
      </div>
      <div data-aos="slide-up" className="article">
        {/* <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div> */}
        {/* <div
          id="div-gpt-ad-3"
          style={{ minWidth: "300px", minHeight: "250px" }}
        >
          <script>
            {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-3'); });`}
          </script>
        </div> */}
        <div className="blog-post">
          <div className="featured-img">
            <img
              style={{ width: "100%" }}
              src="images/Tips-For-Choosing-The-Perfect-Couch.webp"
            />
          </div>

          <div className="post-header">
            {/* <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div> */}
            {/* <div
              id="div-gpt-ad-1"
              style={{ minWidth: "100px", minHeight: "50px" }}
            >
              <script>
                {`googletag.cmd.push(function() {googletag.display('div-gpt-ad-1'); });`}
              </script>
            </div> */}
            <p className="category">Life Style</p>
            <h1>Tips For Choosing The Perfect Couch</h1>
            <img src="images/logo_glance.png" className="glance-logo" />
          </div>

          <div className="post-content">
            <p>
              <em>
                The couch is the most important furniture in the room. So,
                choose wisely!
              </em>
            </p>
            <p>
              Selecting your couch is the most important task when it comes to
              picking out the home furnishings. The biggest chunks of your life
              and time are spent around the couch. Be it with your friends and
              family or just you yourself having a day in your couch, your couch
              would be your best friend through your life. Naturally, a lot of
              thought and consideration should go into the purchasing of your
              couch. Besides, your couch is what your guests first see and sit
              on when they enter your house and that makes it the most important
              piece of furniture in your house. Here are a few tips for
              selecting the perfect couch for your house.
            </p>
            <h6>Your Lifestyle And Your Couch Should Be A Match</h6>
            <p>
              Think about your lifestyle and figure out what is the purpose of
              the couch in your daily life. Do you entertain a lot of people? Do
              you have a big family? Do you have pets? Do you want a couch just
              so you can take a sweet nap in it while watching TV? Everything
              about your lifestyle will decide the type of sofa you should get
              for your house. If you entertain a lot and usually have a lot of
              friends and family over, a big sectional couch would be the one to
              go for. If you mostly stay indoors and hate people, a two sitter
              couch that could perfectly fit you and your pizza box in would do!
            </p>
            <h6>Be Upfront About The Upholstery</h6>
            <p>
              The type of upholstery you want in your sofa will again need to be
              considered carefully. If you are someone with kids and pets, you
              might not want to go for a fabric that’s too high maintenance. On
              the other hand, if you are someone with impeccable taste and would
              like to get some sophisticated look for your sofa, you can go for
              even leather. You need to keep in mind the cleaning codes and the
              maintenance codes of the upholstery fabric before you choose your
              couch.
            </p>
            <h6>A Color That Works With The Space</h6>
            <p>
              Of course, you have to consider the color theme of the space you
              will be keeping your couch in. The color of the couch and the
              space should complement each other. Otherwise, it would just look
              and the couch would seem completely out of place.
            </p>
          </div>

          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Content;
