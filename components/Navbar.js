import React from 'react'
import Image from 'next/image'



const myFunction=()=> {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

const Navbar = () => {
    return (
        <div>
            <div className="header">
                <div className="topnav" id="myTopnav">
                    <a className="logo"><Image src="/images/buzztribe-news-logo.webp" width={180} height={40}/></a>
                    <a className="active">Auto</a>
                    <a>Business</a>
                    <a>Entertainment</a>
                    <a>Food</a>
                    <a>Life Style</a>
                    <a>Travel</a>
                    <a>Sports</a>
                    <a>Health</a>
                    <a className="icon" onClick={myFunction}>
                        <Image src="/images/icon.JPG" width={30} height={30} />
                    </a>
                </div>
            </div>
        </div>
    )
}

export default Navbar
