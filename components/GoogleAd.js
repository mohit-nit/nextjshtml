import React,{useEffect} from 'react'

const GoogleAd = () => {
    useEffect(() => {
        try {
          // @ts-ignore
          (window.adsbygoogle = window.adsbygoogle || []).push({});
        } catch (err) {
          console.error(err);
        }
      }, []);
    return (
        <>
            <ins className="adsbygoogle"
                    style={{display:"block"}}
                    data-ad-client="ca-pub-7421018771149331"
                    data-ad-slot="9071701933"
                    data-ad-format="auto"
                    data-full-width-responsive="true"></ins>            
        </>
    )
}

export default GoogleAd
