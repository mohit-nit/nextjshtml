import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

const Footer = () => {
    return (
        <div>
            <div className="post-footer">
                <p>
                    Article:
                    <Link href="https://www.buzztribe.news/timeline"><a><Image src="/images/buzztribe-news-logo-footer.webp" width={100} height={30}/></a></Link>
                </p>
                <p className="next-art" style={{marginBottom: '25px'}}></p>
            </div>
        </div>
    )
}

export default Footer
