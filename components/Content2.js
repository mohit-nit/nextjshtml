import React from 'react'

const Content2 = () => {
    return (
        <div>
            <div className="article">
                <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div>
                <div className="blog-post">
                    <div className="featured-img">
                        <img style={{ width: '100%' }}
                            src="images/7-Easy-Hacks-to-Decorate-Your-Room-for-the-Festive-Season.webp"
                        />
                    </div>

                    <div className="post-header">
                        <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div>

                        <p className="category">Life Style</p>
                        <h1>Christmas Decor That Can Work All Year</h1>
                        <img src="images/logo_glance.png" className="glance-logo" />
                    </div>

                    <div className="post-content">
                        <p><em>Keep up the spirit all through the year!</em></p>
                        <p>
                            One can never get over the heart fulfilling and joy spreading
                            Christmas decoration. So, why stop at just Christmas? You can
                            ensure that your house is sprinkled with the happy and celebratory
                            vibes of the festival throughout the year without coming across
                            the crazy person who forgot to take their lights down. In fact,
                            you keep your Christmas decoration at the back of your shelf all
                            year only to be used once in a year and that never really makes
                            sense. Think about it –if you had decor pieces that could
                            perfectly work with all seasons and throughout the year, you will
                            get all your money’s worth out of them. Here are a few Christmas
                            decor ideas that you can use all throughout the year.
                        </p>
                        <h6>The Cozy Glow Of The Twinkling Lights</h6>
                        <p>
                            If you are one of those people who absolutely love the warm and
                            cosy hanging lights during Christmas, here’s the good news: you
                            can use them all year. The hanging Christmas tree lights could be
                            used all year round but of course in different manners. For
                            example, you can use them in your bedroom for a little warm and
                            ambient lighting in the night. You can also use them to hang over
                            patio or deck where you can have a romantic dinner or two. One
                            more way you can use them is to turn your kid’s room into a fairy
                            wonderland.
                        </p>
                        <h6>Keeping The Candles Burning All Year</h6>
                        <p>
                            For a change, may be don’t just store away your beautiful
                            Christmas candles this year? It would be best if you get the
                            candles in the versatile white colour instead of the clichéd red
                            and green. That way, you can use the candles all the year long.
                            You can use it for a perfect candle light dinner with your date.
                            You can use it during your meditation when you simply just need to
                            relax, focus and be in the moment. You can also get scented
                            candles for the same purpose and they would work great Christmas
                            or otherwise.
                        </p>
                        <h6>Metallic Pieces Of Decor For All Seasons</h6>
                        <p>
                            May be don’t go for the red and green decor pieces at all! Use
                            metallic colours like silver that can work very well for winter
                            and all through the year. Moreover, pairing metallic colours with
                            neutral themes around the house will amp up the aesthetics of the
                            space.
                        </p>
                    </div>

                    <div className="post-footer">
                        <p>
                            Article:
                            <a href="https://www.buzztribe.news/timeline"
                            ><img src="images/buzztribe-news-logo-footer.webp"
                                /></a>
                        </p>
                        <p className="next-art" style={{ marginBottom: "25px" }}></p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Content2
