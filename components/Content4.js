import React from 'react'

const Content4 = () => {
    return (
        <div>
            <div className="article">
                <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div>
                <div className="blog-post">


                    <div className="featured-img">
                        <img style={{ width: '100%' }} src="images/Useful-tips-for-buying-used-furniture.webp" />
                    </div>

                    <div className="post-header">

                        <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div>

                        <p className="category">Life Style </p>
                        <h1> How To Care For Velvet Furniture</h1>
                        <img src="images/logo_glance.png" className="glance-logo" />
                    </div>

                    <div className="post-content">

                        <p><em>When you get a fancy piece of furniture, you need to give it the fancy treatment.</em></p>
                        <p>Velvet is universally the most stylish and elite options for furniture of all times. It gives a little vintage, a little boho and a lot of glam to the look of your space. However, velvet furniture is also not the easiest to maintain especially if you are new to it. In fact, even people who have been donning velvet furniture for years haven’t got it all right. It is a little difficult to keep in the mint condition. Velvet takes extra and special care and rightfully so. Velvet comes with a price of labour but not something that would make you want to shoot your brains out. Here are a few tips on how to take care of velvet furniture.</p>
                        <h6>Keep It Simple In General</h6>
                        <p>Quite expectedly you will not be able to keep your velvet furniture safe from the usual dirt and debris. In such a case keep the cure usual and simple as well. Simply use a vacuum cleaner to clean the fabric. Make sure to vacuum your velvet furniture at least once a week so as to not let the dirt on it for too long. If not cleaned with a vacuum cleaner at least once a week, the dirt will sit too long on the fabric and soon sediment itself on it. If that happens, it would be a lot difficult for you to clean the dirt out even with a vacuum cleaner.</p>
                        <h6>Speaking For The Spills, Speed It Up</h6>
                        <p>In case there are any spills on you velvet furniture, you need to act fast. Use a clean paper towel or a cloth with good absorbing powers to soak the spill up. However, while you are at it, remember not to dab the spill or rub on it. Doing this will push the liquid further into the deeper layers of the fabric and its fibres. Leave the soaking cloth on the spill for as long as it takes to soak up the entire liquid. Once that’s done, let the fabric air dry.</p>
                        <h6>Keep The Sunlight Away</h6>
                        <p>Velvet is very sensitive to sunlight. If kept in direct sunlight, the velvet’s colour would fade away in a matter of time. Select a space with low sunlight for your velvet furniture or at least cover it with a throw for protecting its colour.</p>

                    </div>

                    <div className="post-footer">
                        <p>Article: <a href="https://www.buzztribe.news/timeline"><img src="images/buzztribe-news-logo-footer.webp" /></a></p>
                        <p className="next-art" style={{ marginBottom: "25px" }}> </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Content4
