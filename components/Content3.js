import React from 'react'

const Content3 = () => {
    return (
        <div>
            <div className="article">

                <div className="ad-placement">
                    <img src="images/ad1.JPG" />
                </div>
                <div className="blog-post">


                    <div className="featured-img">
                        <img style={{ width: '100%' }} src="images/GO-BANANA-4-DESSERT-MADE-OUT-OF-BANANA.webp" />
                    </div>

                    <div className="post-header">

                        <div className="ad-placement">
                            <img src="images/ad2.JPG" />
                        </div>

                        <p className="category">Food</p>
                        <h1>Desserts That Will Not Make You Fat</h1>
                        <img src="images/logo_glance.png" className="glance-logo" />
                    </div>

                    <div className="post-content">

                        <p><em>Indulge in some desserts guilt free!</em></p>
                        <p>For every time that you have stopped yourself from taking that bite out of a good piece of chocolate cake, you can now make up to them. There’s nothing wrong with having indulging in some dessert even though you are looking to maintain or lose your weight. There are plenty of desserts out there that even people who gain weight by just inhaling air can enjoy. The obvious trick is to moderate your portions. However, if you are still worried about the empty calories, we have some news for you. There are desserts, and we say this with conviction, that will not make you fat. Here are a few desserts that you can enjoy without having to worry about gaining those extra pounds.</p>
                        <h6>Bananas Dipped In Dark Chocolate</h6>
                        <p>You want some health and taste in your dessert? Go with bananas and of course, chocolate.&nbsp; Bananas have incredible benefits for your health and they have the sweetness to them which would take care of the no sugar part in your healthy, guilt free dessert. The sinfulness like in any good dessert would of course come from the dark chocolate. Just dip some bananas in dark chocolate that’s melted and mixed with almonds and walnuts. Follow this step by refrigerating the dark chocolate coated bananas like a popsicle for an hour or two and your dessert will be ready.</p>
                        <h6>A Fudgy Delight For A Sinless Dessert</h6>
                        <p>Who said fudgy brownies are bad for losing weight? We say have all the fudge in your life as you would like but with a twist. Make the fudgy brownies with a healthy amount of raspberries and some lemon zest for garnish. You use applesauce for sweetness and whole wheat flour. That instantly makes it a lot lighter in terms if calories compared to your normal brownies. Moreover, there’s no compromise in taste as you get big and bold flavours that could even possibly send you to a dessert coma. You indulge while still maintaining your nutrition and calorie goals.</p>
                        <h6>All the Goodness Of The Carrots</h6>
                        <p>Okay, we agree that a little bit of sugar is important in carrot cakes, but that amount barely does any harm. Instead of using the cream cheese frosting use Greek yoghurt for the frosting and the carrot cake will taste a hundred percent as good as your OG grandma special carrot cake, we promise!</p>

                    </div>

                    <div className="post-footer">
                        <p>Article: <a href="https://www.buzztribe.news/timeline"><img src="images/buzztribe-news-logo-footer.webp" /></a></p>
                        <p className="next-art" style={{ marginBottom: "25px" }}> </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Content3
