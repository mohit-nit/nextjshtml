import { useEffect, useState } from 'react'

import { adDevice } from "../lib/dfp/const"

export default function AdSlot (props) {
  const { id, customStyle } = props
  const [showAdd, setShowAdd] = useState(false)
  
  useEffect(() => {
    if ((window.screen.width < 768 && adDevice[id] !== "d") || 
        (window.screen.width >= 768 && adDevice[id] !== "m"))
          setShowAdd(true)
    
    return () => {}
  }, [])

  return (
    showAdd ? <div id={id} style={customStyle} /> : <></>
  )
}